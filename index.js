// File: index.js

var search_base_url =
  "https://api.spotify.com/v1/search?query={artist_name}&type=artist&market=US&offset=0&limit=20";

// The following are just stored artist IDs for verification -- nothing to do here
//var artist_id = "0TnOYISbd1XYRBk9myaseg";  // Enrique Iglesias
//var artist_id = "3t58jfUhoMLYVO14XaUFLA";  // Pat Metheny
//var artist_id = "5olDKSsFhhmwh8UCWwKtpq";  // Chick Corea
//var artist_id = "6v0d6b30Aw6lK4AtZuOElo";  // Cuong Vu
//var artist_id = "31TPClRtHm23RisEBtV3X7";  // Timberlake

// ==============================================================================
// TASK 1: Obtain your own OAuth Token and replace this one -- this "Token" 
//         will be expired by exam time and will NOT work -- just a sample!
//         (Note: Keep refreshing your Token about every 30 mins)
// ==============================================================================
var default_OAuthToken =
  "BQBk_W2Ut8-1G20ZLmRiqsvnKo-zS4yZbpJT7hgpLru0Wdqhmsp3kyXb1DH8AcZ5BRw72UX-Jo6MtrYQjwNOTYr_eLKi9kUj1OKoK-7W8tdJmhDyTr79risUgZ0uNOJSZGN8lfu16mjoB9oWxO3cOmmcI3BHAAzVWg";

// =======================================================================================
// TASK 2: Complete and test this function that inserts an artist name into search url.  
//         Use a JavaScript REGEX search and replace the {artist_name} template in the 
//         above search_base_url use this function to concantenate (build) your search url
// =======================================================================================
function getSearchUrl(name) {
  var url = search_base_url;
  var aname = $("input#artist-name").val()
  url = url.replace('{artist_name}', aname)
  return url;
}

function api_search(artist_name, auth_token) {
  let oAuthToken = auth_token;
  let myurl = getSearchUrl(artist_name);
  console.log("searchURL: " + myurl);
  
  // =========================================================================
  // TASK 3: After verifying that the correct search URL is present 
  //         after the api_search() is called, use JQuery $(select).manipulate
  //         to store this url in the input field selected by "input#search-url"        
  // =========================================================================
    
  // AJAX call to get the Spotify Search Result  
  $.ajax({
    method: "GET",
    url: myurl,
    dataType: "json",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + oAuthToken
    },
    success: function(data) {
      console.log("success");
      console.log(data);
      
      // =========================================================================
      // TASK 4: - Add code to convert the raw data object into a
      //           JSON string before storing to the element identified by
      //           "textarea#spotify-data".  (This will be a useful string to
      //           copy-paste into an editor like VS Code to examine JSON data)
      // =========================================================================
      var myJSON = JSON.stringify(data);
      $("textarea#spotify-data").text(myJSON);
      let out = data.artists.items[0].name;
      let followers = data.artists.items[0].followers.total;
      let genre = data.artists.items[0].genres;
      let url = data.artists.items[0].external_urls.spotify;
      console.log("Name:" + out);
      console.log("Followers:" + followers);
      console.log("Genre:" + genre);
      console.log("URL:" + url);
      $("span#out-name").text(out);
      $("span#followers").text(followers);
      $("span#genre").text(genre);
      $("span#artist-url").text(url);
      document.getElementById("col").innerHTML="<img src = "+ data.artists.items[0].images[0].url +">";
    },
    error: function(e) {
      let err = JSON.stringify(e);
      console.log(err);
    },
    cache: false
  });
}


$( document ).ready()
$(function() {
  console.log("Starting...");

  // Handle the Send API Request Button Click
  $("#send-req").click(function() {
    console.log("Send Button Hit!");
    let artist_name = $("#artist-name").val();
    console.log("Artist ID: " + artist_name);

    // Get default or input OAuth Token
    let auth_token = default_OAuthToken;
    if ($("input#auth-token").val() !== "") {
      auth_token = $("input#auth-token").val();
    }
    
    // Display the OAuth Token for us to see
    console.log("OAuth Token: " + auth_token);
    $("input#auth-token").val(auth_token);
    api_search(artist_name, auth_token);
  });
});